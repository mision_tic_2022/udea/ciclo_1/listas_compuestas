'''
    Desarrolle una función que solicite los nombres de 
    5 estudiantes y los guarde en el indice cero de una lista 
    compuesta, posteriormente solicite las notas de cada estudiante, 
    estas notas debe almacenarlas en el indice uno de la lista compuesta,
    las notas deben estar relacionadas al indice en el que se encuentran 
    los nombres de los estudiantes, ejemplo:
        curso = [
                    ['Juan', 'Juliana', 'María', 'Andrés', 'Pedro'],
                    [4.5, 4.5, 4.2, 4.1, 4.8]
                ]
    La función debe retornar la lista compuesta con los datos de los estudiantes
'''

#------------------Soluciones de algunos Estudiantes:-------------------
#Solución 1
def solicitarNombres():
    Nombres = []
    for i in range(0, 5):
        nombre = input("Ingrese el nombre: ")
        Nombres.append(nombre)
    return Nombres

def solicitarNotas():
    Notas = []
    for i in range(0, 5):
        nota = float(input("Ingrese la nota: "))
        Notas.append(nota)
    return Notas


def cursoGeneral():
    curso=[solicitarNombres(), solicitarNotas()]
    print(curso)
    return curso

#Solución 2
cursos = []
estudiantes = []
for x in range(5):
    controlStr = input('ingrese nombre de estudiante ')
    estudiantes.append(controlStr)
notas = []
for x in range(5):
    nota = input('ingrese la nota de ' + estudiantes[x] + ' ')
    notas.append(nota)

cursos.append(estudiantes)
cursos.append(notas)

print(cursos)

#Solución 3
cont = 4
estudiantes = []
notas = []
while cont >=0:
    estudiante = input('Ingrese el nombre del estudiante: ')
    estudiantes.append(estudiante)
    nota = input('Ingrese la nota del estudiante: ')
    notas.append(nota)
    cont-=1
curso=[]
curso.append(estudiantes)
curso.append(notas)
print(curso)

#Solución 4
def crear_curso():
    curso = []
    nombres = []
    notas = []
    decision = "s"
    while decision == "s":
        nombre = input("\nDigite el nombre del estudiante: ").capitalize
        nota = float(input("Digite la nota del estudiante: "))
        nombres.append(nombre)
        notas.append(nota)
        decision = input("\nDesea agregar otro estudiante? s/n\n>>> ")
    curso.append(nombres)
    curso.append(notas)
    
    return curso

#Solución 5
cont = 4
curso=[
    [],
    []
]
while cont >=0:
    estudiante = input('Ingrese el nombre del estudiante: ')
    curso[0].append(estudiante)
    nota = input('Ingrese la nota del estudiante: ')
    curso[1].append(nota)
    cont-=1
print(curso)