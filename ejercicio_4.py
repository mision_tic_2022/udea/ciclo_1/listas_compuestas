
notas = [
    [
        [4.2, 5.0, 3.2, 4.5],
        [4.5, 4.0, 4.2, 3.5]
    ],
    [
        [3.2, 4.8, 3.9, 4.2],
        [3.5, 4.9, 4.2, 3.1]
    ]
]

padre = notas[0]
print(padre)
hijo = padre[0]
print(hijo)
elemento = hijo[0]
print(elemento)

elemento = notas[0][0][1]
print(elemento)

'''
Desarrolle una función que reciba como parámetro la lista compuesta 'notas',
retorne una tupla con el promedio de notas de cada 'lista nieto', promedio 
de notas de cada 'lista padre' y promedio general de notas
    ([4.5, 3.2, 4.9, 3.5], [4.4, 4.2], 4.1)
'''
#Solución de Andrés Felipe
def promedios(lista: list):
    prom_nietos = []
    prom_hijos = []
    suma_gen = 0
    cont_gen = 0    
    for hijo in lista:
        suma_hijo = 0
        cont_hijo = 0
        for nieto in hijo:
            suma_nieto = 0
            tamanio_nieto = len(nieto)
            for elemento in nieto:
                suma_gen += elemento
                suma_nieto += elemento
                suma_hijo += elemento
                cont_hijo += 1
                cont_gen += 1
            suma_nieto /= tamanio_nieto
            prom_nietos.append(suma_nieto)
        suma_hijo /= cont_hijo
        prom_hijos.append(suma_hijo)
    suma_gen /= cont_gen
    
    return prom_nietos,prom_hijos,suma_gen

#Solución de Jesús
def promedio_notas(lista):
    promedio_gen=[]
    promedios_nieto=[]
    promedios_hijos=[]
    for hijo in lista:
        promedio_hijo=0
        for nieto in hijo:
            promedio_nieto = sum(nieto)/len(nieto)
            promedios_nieto.append(promedio_nieto)
            promedio_hijo+=promedio_nieto
        promedios_hijo=promedio_hijo/len(hijo)
        promedios_hijos.append(promedios_hijo)
    promedio_gen.append(promedios_nieto)
    promedio_gen.append(promedios_hijos)
    prom_general=sum(promedios_hijos)/len(promedios_hijos)
    promedio_gen.append(prom_general)

    return tuple(promedio_gen)

#print(promedio_notas(notas))

#Solución de Kevin Santiago
def promedioNotas(notasList):

    arr = []
    arr2 = []
    arr3 = []

    for x in notasList:
        for y in x:
            arr.append(sum(y)/len(y))

    for x in notasList:
        arr2.append(sum(list(value for values in x for value in values)) /
                    len(list(value for values in x for value in values)))

    arr3.append(sum(list(value for value in arr2)) /
                len(list(value for value in arr2)))

    return (arr, arr2, arr3)