notas = [
    [4.5, 3.8, 2.2],
    [2.1, 4.9, 4.5]
]
'''
Calcular el promedio de notas de cada 'lista hijo' y 
obtener el promedio general de notas
'''
promedio_1 = (notas[0][0] + notas[0][1]+ notas[0][2]) / len(notas[0])
promedio_2 = (notas[1][0] + notas[1][1]+ notas[1][2]) / len(notas[1])

promedio_general = (promedio_1 + promedio_2)/2

print(promedio_1)
print(promedio_2)
print(promedio_general)