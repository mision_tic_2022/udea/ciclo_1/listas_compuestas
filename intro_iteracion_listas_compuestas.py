numeros = [
    [100, 200, 300],
    [80, 30, 250],
    [90, 20, 150],
    [70, 10, 350],
    [60, 90, 450]
]

suma = 0
for lista in numeros:
    print(lista)
    for elemento in lista:
        suma += elemento
        #print(elemento)
print(suma)

'''
Desarrolle una función que reciba como parámetro la lista compuesta 'números',
retorne una tupla donde el primer elemento sea una lista con los promedios 
de cada 'lista hijo' y el segundo elemento sea el promedio general.
NOTA: Utilizar ciclos anidados
    ([250, 200, ...], 400)
'''

numeros = [
    [100, 200, 300],
    [80, 30, 250],
    [90, 20, 150],
    [70, 10, 350],
    [60, 90, 450]
]

def promedios(lista: list):
    lista_proms = []
    suma_gen = 0
    cont_gen = 0
    for sub in lista:
        suma_par = 0
        cont_par = 0
        for elemento in sub:
            suma_gen += elemento
            suma_par += elemento
            cont_par += 1
            cont_gen += 1
        suma_par /= cont_par
        lista_proms.append(suma_par)
    suma_gen /= cont_gen
    
    return lista_proms,suma_gen
