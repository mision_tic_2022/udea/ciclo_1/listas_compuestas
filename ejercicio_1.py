numeros = [
    [100, 200, 300],
    [80, 90, 250]
]


'''
Desarrollar una función que reciba como parámetro la lista 'números' y retorne 
una tupla donde el primer valor es una lista con la sumatoria total de los 
valores de cada lista hijo y el segundo valor la sumatoria total de la lista padre.
    Ejemplo:
        ([600, 420], 1020)
'''

def sumatoria(numeros: list)->tuple:
    n1 = numeros[0][0]
    n2 = numeros[0][1]
    n3 = numeros[0][2]
    suma_hijo_1 = n1+n2+n3
    suma_hijo_2 = numeros[1][0] + numeros[1][1] + numeros[1][2]
    suma_total = suma_hijo_1 + suma_hijo_2
    #Retornar tupla
    return ([suma_hijo_1, suma_hijo_2], suma_total)